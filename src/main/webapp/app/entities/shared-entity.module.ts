import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Training01SharedModule } from 'app/shared';
import { DataViewModule } from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { ListboxModule } from 'primeng/listbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PanelModule } from 'primeng/panel';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CardModule } from 'primeng/card';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CurrencyMaskModule } from 'ng2-currency-mask';

/* jhipster-needle-import-entity-as-list - JHipster will add entity modules imports here */

@NgModule({
  imports: [
    Training01SharedModule,
    RouterModule,
    // primeng
    DataViewModule,
    TableModule,
    CalendarModule,
    ListboxModule,
    AutoCompleteModule,
    PanelModule,
    DialogModule,
    CheckboxModule,
    ConfirmDialogModule,
    CardModule,
    // ngx
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    // others
    CurrencyMaskModule
  ],
  // prettier-ignore
  declarations: [
        /* jhipster-needle-declaration-entity-as-list */
    ],
  entryComponents: [],
  // prettier-ignore
  exports: [
        /* jhipster-needle-as-list-export-shared-module - JHipster will add entity exports imports here */
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Training01SharedEntityModule {}
