export const navigation = [
  {
    name: 'Dashboard',
    url: '',
    icon: 'icon-speedometer',
    authorities: ['ROLE_ADMIN']
  },
  {
    name: 'Entities',
    url: '',
    icon: 'icon-speedometer',
    authorities: ['ROLE_USER'],
    children: [
      {
        name: 'Home',
        url: '/',
        icon: 'icon-user',
        authorities: ['ROLE_USER']
      }
      // ---- jhipster-needle-add-entity-to-nav-menu -----------
    ]
  },
  {
    name: 'Administration',
    icon: 'icon-user',
    url: '',
    authorities: ['ROLE_ADMIN'],
    children: [
      {
        name: 'User Management',
        url: '/admin/user-management',
        icon: 'icon-user'
      },
      {
        name: 'User Tracker',
        url: '/admin/jhi-tracker',
        icon: 'icon-user'
      },
      {
        name: 'User Metric',
        url: '/admin/jhi-metrics',
        icon: 'icon-user'
      },
      {
        name: 'Health',
        url: '/admin/jhi-health',
        icon: 'icon-user'
      },
      {
        name: 'Configuration',
        url: '/admin/jhi-configuration',
        icon: 'icon-user'
      },
      {
        name: 'Audits',
        url: '/admin/audits',
        icon: 'icon-user'
      },
      {
        name: 'Logs',
        url: '/admin/logs',
        icon: 'icon-user'
      },
      {
        name: 'Docs',
        url: '/admin/docs',
        icon: 'icon-user'
      }
    ]
  }
];
