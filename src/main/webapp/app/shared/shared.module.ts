import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { Training01SharedLibsModule, Training01SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

import {
  EditViewDirective,
  CardViewDirective,
  ItemViewDirective,
  SimpleViewDirective,
  HeaderViewDirective,
  FooterViewDirective
} from 'app/shared/base/abstract-entity-view.directive';

@NgModule({
  imports: [Training01SharedLibsModule, Training01SharedCommonModule],
  declarations: [
    JhiLoginModalComponent,
    HasAnyAuthorityDirective,
    EditViewDirective,
    CardViewDirective,
    ItemViewDirective,
    SimpleViewDirective,
    HeaderViewDirective,
    FooterViewDirective
  ],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
  entryComponents: [JhiLoginModalComponent],
  exports: [
    Training01SharedCommonModule,
    JhiLoginModalComponent,
    HasAnyAuthorityDirective,
    EditViewDirective,
    CardViewDirective,
    ItemViewDirective,
    SimpleViewDirective,
    HeaderViewDirective,
    FooterViewDirective
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Training01SharedModule {
  static forRoot() {
    return {
      ngModule: Training01SharedModule
    };
  }
}
