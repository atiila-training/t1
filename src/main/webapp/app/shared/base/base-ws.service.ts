import { Observable, Subscription } from 'rxjs';

import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import { AuthServerProvider } from 'app/core';
import { WindowRef } from 'app/core/tracker/window.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class BaseWebSocketService {
  stompClient = null;
  connection: Promise<any>;
  connectedPromise: any;
  listener: Observable<any>;
  private subscription: Subscription;

  constructor(protected authServerProvider: AuthServerProvider, protected $window: WindowRef) {
    this.connection = this.createConnection();
  }

  connect() {
    if (this.connectedPromise === null) {
      this.connection = this.createConnection();
    }
    // building absolute path so that websocket doesn't fail when deploying with a context path
    const loc = this.$window.nativeWindow.location;
    let urlApps;
    urlApps = '//' + loc.host + loc.pathname + 'websocket/message';
    const authToken = this.authServerProvider.getToken();
    if (authToken) {
      urlApps += '?access_token=' + authToken;
    }
    const socket = new SockJS(urlApps);
    this.stompClient = Stomp.over(socket);
    const headers = {};
    this.stompClient.connect(
      headers,
      () => {
        this.connectedPromise('success');
        this.connectedPromise = null;
      }
    );
  }

  disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
      this.stompClient = null;
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  private createConnection(): Promise<any> {
    return new Promise((resolve, reject) => (this.connectedPromise = resolve));
  }
}
