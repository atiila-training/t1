#!/bin/sh
DONE_INIT="DONE_INIT"
if [ ! -e $DONE_INIT ]; then
    echo "Initialize system....."
    sudo chown -R jhipster /home/jhipster
	touch $DONE_INIT
else
    echo "The application will start in ${JHIPSTER_SLEEP}s..." && sleep ${JHIPSTER_SLEEP}
fi
exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "${HOME}/app.war" "$@"
