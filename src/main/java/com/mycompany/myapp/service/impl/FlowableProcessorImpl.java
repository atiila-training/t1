package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mycompany.myapp.service.TaskProcessService;
import org.apache.camel.Header;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.EventSubscription;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.apache.camel.Body;
import org.flowable.task.api.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.dto.TaskDTO;

@Component
@Transactional
public class FlowableProcessorImpl implements TaskProcessService {

    private final Logger log = LoggerFactory.getLogger(FlowableProcessorImpl.class);

    @Autowired
    protected RepositoryService repositoryService;

    @Autowired
    protected RuntimeService runtimeService;

    @Autowired
    protected TaskService taskService;

    public Map<String, Object> getVariables(String id, Object value) {
        Map<String, Object> result = new HashMap<>();
        result.put(id, value);
        return result;
    }

    public Map<String, Object> getVariables() {
        Map<String, Object> result = new HashMap<>();
        return result;
    }

    public ProcessInstance startProcess(String processName, Map<String, Object> variables) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, variables);
        return p;
    }

    public ProcessInstance getProcessInstance(String processName, String key) {
        ProcessInstance p =  runtimeService.createProcessInstanceQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(key)
            .singleResult();
        return p;
    }

    public ProcessInstance cancelProcessInstance(String processName, String key) {
        ProcessInstance p =  runtimeService.createProcessInstanceQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(key)
            .singleResult();

        if (p != null) {
            runtimeService.deleteProcessInstance(p.getId(), "User Canceled");
        }
        return p;
    }

    public ProcessInstance startProcess(String processName, String key) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, key);
        return p;
    }

    public ProcessInstance startProcess(String processName, String key, Map<String, Object> variables) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, key, variables);
        return p;
    }

    public List<Task> getTasks(String processName, String bussinesKey) {
        return taskService.createTaskQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey)
            // .taskCandidateOrAssigned(SecurityUtils.getCurrentUserLogin().get())
            .list();
    }

    @Override
    public List<Task> getTasks(String id) {
        return taskService.createTaskQuery()
            .processInstanceId(id)
            // .taskCandidateOrAssigned(SecurityUtils.getCurrentUserLogin().get())
            .list();
    }

    public List<EventSubscription> getEventSubscriptions(String id) {
        return runtimeService.createEventSubscriptionQuery()
            .processInstanceId(id)
            .list();
    }

    public List<Execution> getExecutions(String processName, String bussinesKey) {
        return runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey, true)
            .list();
    }

    @Override
    public List<Execution> getExecutions(String id) {
        return runtimeService.createExecutionQuery()
            .processInstanceId(id)
            .list();
    }

    public Task getTask(String id) {
        return taskService.createTaskQuery()
            .taskId(id)
            .singleResult();
    }

    public boolean completeTask(String taskId) {
        Boolean result = false;
        Task task = taskService.createTaskQuery()
            .taskId(taskId)
            .singleResult();

        if (task != null) {
            if (task.getAssignee() == null)
                taskService.claim(taskId, SecurityUtils.getCurrentUserLogin().get());

            taskService.complete(taskId);
            result = true;
        }
        return result;
    }

    public void completeTask(String processName, String bussinesKey, String taskName) {
        List<Task> tasks = taskService.createTaskQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey)
            .taskDefinitionKey(taskName)
            .list();

        if (tasks != null) {
            for (Task task: tasks) {
                if (task.getAssignee() == null)
                    taskService.claim(task.getId(), SecurityUtils.getCurrentUserLogin().get());
                taskService.complete(task.getId());
            }
        }
    }

    public void completeSignalEvent(String processName, String bussinesKey, String eventName) {
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey, true)
            .signalEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.signalEventReceived(eventName, exec.getId());
        }
    }

    public void completeSignalEvent(String id) {
        EventSubscription event = runtimeService.createEventSubscriptionQuery().id(id).singleResult();
        if (event != null) {
            runtimeService.signalEventReceived(event.getExecutionId());
        }
    }

    public void completeMessageEvent(String processName, String bussinesKey, String eventName) {
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey, true)
            .messageEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.messageEventReceived(eventName, exec.getId());
        }
    }

    public void completeMessageEvent(String id) {
        EventSubscription event = runtimeService.createEventSubscriptionQuery().id(id).singleResult();
        if (event != null) {
            runtimeService.messageEventReceived(event.getEventName(), event.getExecutionId());
        }
    }

    public void receiveCompleteTask(@Body Map<String, Object> vars,
                                    @Header(TaskProcessService.PROCESS_NAME) String processName,
                                    @Header(TaskProcessService.BUSSINES_KEY) String bussinesKey,
                                    @Header(TaskProcessService.EVENT_NAME) String taskName) {

        if (vars == null) vars = getVariables();
        List<Task> tasks = taskService.createTaskQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey)
            .taskDefinitionKey(taskName)
            .list();

        if (tasks != null) {
            for (Task task: tasks) {
                if (task.getAssignee() == null)
                    taskService.claim(task.getId(), SecurityUtils.getCurrentUserLogin().get());
                taskService.complete(task.getId(), vars);
            }
        }
    }

    public void receiveCompleteMessageEvent(@Body Map<String, Object> vars,
                                            @Header(TaskProcessService.PROCESS_NAME) String processName,
                                            @Header(TaskProcessService.BUSSINES_KEY) String bussinesKey,
                                            @Header(TaskProcessService.EVENT_NAME) String eventName) {

        if (vars == null) vars = getVariables();
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey, true)
            .messageEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.messageEventReceived(eventName, exec.getId(), vars);
        }
    }

    public void receiveCompleteSignalEvent(@Body Map<String, Object> vars,
                                           @Header(TaskProcessService.PROCESS_NAME) String processName,
                                           @Header(TaskProcessService.BUSSINES_KEY) String bussinesKey,
                                           @Header(TaskProcessService.EVENT_NAME) String eventName) {

        if (vars == null) vars = getVariables();
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey, true)
            .signalEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.signalEventReceived(eventName, exec.getId(), vars);
        }
    }

    public void receiveCreateProcess(@Body Map<String, Object> vars,
                                     @Header(TaskProcessService.PROCESS_NAME) String processName,
                                     @Header(TaskProcessService.BUSSINES_KEY) String bussinesKey,
                                     @Header(TaskProcessService.EVENT_NAME) String eventName) {

        if (vars == null) vars = getVariables();
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, bussinesKey, vars);
    }

    public List<TaskDTO> getTaskWithMessages(String name, String id) {
        ProcessInstance p = getProcessInstance(name, id);
        List<TaskDTO> taskDTOS = new ArrayList<>();

        if (p != null) {
            List<Task> tasks = getTasks(name, id);
            List<EventSubscription> subscriptions = getEventSubscriptions(p.getId());

            for (Task item : tasks) {
                TaskDTO taskDTO = new TaskDTO();
                taskDTO.setId(item.getId());
                taskDTO.setName(item.getName());
                taskDTO.setDefinitionKey(item.getTaskDefinitionKey());
                taskDTO.setFormKey(item.getFormKey());
                taskDTO.setAssignee(item.getAssignee());
                taskDTO.setTaskType(1);
                taskDTOS.add(taskDTO);
            }

            if (!subscriptions.isEmpty()) {
                for (EventSubscription item : subscriptions) {
                    TaskDTO taskDTO = new TaskDTO();
                    taskDTO.setId(item.getId());
                    taskDTO.setName(item.getEventName());
                    taskDTO.setDefinitionKey(item.getActivityId());
                    if (item.getEventType().equals("message")) {
                        taskDTO.setTaskType(3);
                    } else taskDTO.setTaskType(2);
                    taskDTOS.add(taskDTO);
                }
            }
        }
        return taskDTOS;
    }

    @Override
    public void processTaskByType(Integer taskType, String id) {
        switch (taskType) {
            case 1: {
                completeTask(id);
                break;
            }
            case 2: {
                completeSignalEvent(id);
                break;
            }
            case 3: {
                completeMessageEvent(id);
                break;
            }
        }
    }

}
