package com.mycompany.myapp.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.mycompany.myapp.config.JasperConfiguration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

@Component
public class ReportUtils implements InitializingBean {

    private String reportWorkPath;

    private String reportPath;

    @Autowired
    private JRSwapFileVirtualizer sfv;

    @Autowired
    private Environment env;

    @Autowired
    private DataSource datasource;

    @Autowired
    private ResourceLoader loader;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.reportWorkPath = env.getProperty(JasperConfiguration.REPORT_WORK_PATH, "/tmp");
        this.reportPath = env.getProperty("application.report-path", "classpath:/public/report");

        File file = new File(this.reportWorkPath);
        if (!file.exists()) file.mkdirs();
    }

    public String getRandomFileName() {
        return this.reportWorkPath + "/" + UUID.randomUUID().toString();
    }

    public JasperReport loadRreport(String name) throws JRException, IOException {
        // Ambil default jasper
        Resource jasper = loader.getResource(reportPath + "/"  + name + ".jasper");
        // Jika file jasper tidak di temukan
        if (!jasper.exists()) {
            jasper = loader.getResource(this.reportWorkPath + "/"  + name + ".jasper");
        }
        JasperReport jr = null;
        if (jasper.exists()) {
            jr = (JasperReport) JRLoader.loadObject(jasper.getInputStream());
        } else {
            Resource jrxml = loader.getResource(reportPath + "/"  + name + ".jrxml");
            jr = JasperCompileManager.compileReport(jrxml.getInputStream());
            JRSaver.saveObject(jr, jasper.getFilename());
        }
        return jr;
    }

    public Map<String, Object> getReportParam() {
        Map<String, Object> r = new HashMap<>();
        String port = env.getProperty("server.port");
        String addr = InetAddress.getLoopbackAddress().getHostName();

        r.put(JRParameter.REPORT_VIRTUALIZER, sfv);
        r.put("imageUrl", "http://" + addr + ":" + port + "/images/");
        return r;
    }

    public Map<String, Object> getReportParam(HttpServletRequest request) {
        Map<String, Object> r = new HashMap<>();
        String scheme = request.getScheme();
        int port = request.getServerPort();
        String addr = request.getServerName();

        r.put(JRParameter.REPORT_VIRTUALIZER, sfv);
        r.put("imageUrl", scheme + "://" + addr + ":" + port + "/images/");
        return r;
    }

    public ResponseEntity<InputStreamResource> generatePDF(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = getRandomFileName() + ".pdf";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(loadRreport(reportName), params, conn);

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("atiila");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(tmpFileName);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
            responseHeaders.setContentDispositionFormData("inline", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }


    public ResponseEntity<InputStreamResource> generateXLSX(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = getRandomFileName() + ".xlsx";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(loadRreport(reportName), params, conn);

            SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
            reportConfig.setSheetNames(new String[] { name });

            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(tmpFileName);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.setConfiguration(reportConfig);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            responseHeaders.setContentDispositionFormData("attachment", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }


    public ResponseEntity<InputStreamResource> generateCSV(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = getRandomFileName() + ".csv";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(loadRreport(reportName), params, conn);

            SimpleWriterExporterOutput output = new SimpleWriterExporterOutput(tmpFileName);

            JRCsvExporter exporter = new JRCsvExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("text/csv"));
            responseHeaders.setContentDispositionFormData("file", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    public ResponseEntity<InputStreamResource> generateHTML(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = getRandomFileName() + ".html";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(loadRreport(reportName), params, conn);

            SimpleHtmlExporterOutput output = new SimpleHtmlExporterOutput(tmpFileName);

            HtmlExporter exporter = new HtmlExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("text/csv"));
            responseHeaders.setContentDispositionFormData("file", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

}
