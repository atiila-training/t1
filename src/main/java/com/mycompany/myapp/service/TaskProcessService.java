package com.mycompany.myapp.service;

import org.flowable.engine.runtime.EventSubscription;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mycompany.myapp.service.dto.TaskDTO;

public interface TaskProcessService {

    public static String PROCESS_NAME = "PROCESS_NAME";
    public static String BUSSINES_KEY = "BUSSINES_KEY";
    public static String EVENT_NAME = "EVENT_NAME";

    public Map<String, Object> getVariables(String id, Object value);

    public Map<String, Object> getVariables();

    public ProcessInstance startProcess(String processName, Map<String, Object> variables);

    public ProcessInstance getProcessInstance(String processName, String key);

    public ProcessInstance cancelProcessInstance(String processName, String key);

    public ProcessInstance startProcess(String processName, String key);

    public ProcessInstance startProcess(String processName, String key, Map<String, Object> variables);

    public List<Task> getTasks(String processName, String bussinesKey);

    public List<Task> getTasks(String id);

    public List<EventSubscription> getEventSubscriptions(String id);

    public List<Execution> getExecutions(String processName, String bussinesKey);

    public List<Execution> getExecutions(String id);

    public Task getTask(String id);

    public boolean completeTask(String taskId);

    public void completeTask(String processName, String bussinesKey, String taskName);

    public void completeSignalEvent(String processName, String bussinesKey, String eventName);

    public void completeSignalEvent(String id);

    public void completeMessageEvent(String processName, String bussinesKey, String eventName);

    public void completeMessageEvent(String id);

    public void receiveCompleteTask(Map<String, Object> vars,
                                    String processName,
                                    String bussinesKey,
                                    String taskName);

    public void receiveCompleteMessageEvent(Map<String, Object> vars,
                                            String processName,
                                            String bussinesKey,
                                            String eventName);

    public void receiveCompleteSignalEvent(Map<String, Object> vars,
                                           String processName,
                                           String bussinesKey,
                                           String eventName);

    public void receiveCreateProcess(Map<String, Object> vars,
                                     String processName,
                                     String bussinesKey,
                                     String eventName);

    public List<TaskDTO> getTaskWithMessages(String name, String id);

    public void processTaskByType(Integer taskType, String id);
}
