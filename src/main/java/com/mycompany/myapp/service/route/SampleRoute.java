package com.mycompany.myapp.service.route;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SampleRoute extends SpringRouteBuilder {

    @Override
    public void configure() throws Exception {

        from("timer:foo?period=5m")
            .transform()
            .simple("Check Is Camel Running ${date:now:yyyy-MM-dd HH:mm:ss}")
            .to("log:out");
    }
}
