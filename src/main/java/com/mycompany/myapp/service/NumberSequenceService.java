package com.mycompany.myapp.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.mycompany.myapp.domain.NumberSequence;
import com.mycompany.myapp.repository.NumberSequenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumberSequenceService {

    @Autowired
    private NumberSequenceRepository numberSequenceRepository;

    @Autowired
    private HazelcastInstance hzInstance;

    public Long getNextValue(String tag, String value) {
        Long result = 0l;
        ILock lock = hzInstance.getLock(tag+ "-" + value);
        lock.tryLock();
        try {
            NumberSequence number = numberSequenceRepository.queryNumber(tag, value);
            if (number == null) {
                number = new NumberSequence();
                number.setTag(tag);
                number.setValue(value);
                number.setNextValue(0l);
                number = numberSequenceRepository.save(number);
            }

            // Increment
            result = number.getNextValue() + 1l;

            // Update Number
            number.setNextValue(result);
            number = numberSequenceRepository.save(number);

        } finally {
            lock.unlock();
        }
        return result;
    }

    public String getNextValue(String tag, String value, String format, Object... params) {
        return String.format(format, getNextValue(tag, value), params);
    }

    public String getRadixNextValue(String tag, String value, String format, int radix, int length) {
        Long nextResult = getNextValue(tag, value);
        return String.format(format, leftPad(Long.toString(nextResult, radix).toUpperCase(), length, '0'));
    }

    public static String leftPad(String value, int length, char padchar) {
        StringBuilder sb = new StringBuilder();
        while (sb.length() + value.length() < length) {
            sb.append(padchar);
        }
        sb.append(value);
        return sb.toString();
    }

    public static String rightPad(String value, int length, char padchar) {
        StringBuilder sb = new StringBuilder();
        sb.append(value);
        while (sb.length() + value.length() < length) {
            sb.append(padchar);
        }
        return sb.toString();
    }

}
