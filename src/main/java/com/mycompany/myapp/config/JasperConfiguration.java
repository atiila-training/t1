package com.mycompany.myapp.config;

import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.io.File;

@Configuration
public class JasperConfiguration implements InitializingBean {

    public static String REPORT_WORK_PATH = "application.report-work-path";

    @Autowired
    private Environment env;

    private String workPath;

    @Override
    public void afterPropertiesSet() throws Exception {
        workPath = env.getProperty(REPORT_WORK_PATH, "/tmp");

        File file = new File(workPath);
        if (!file.exists()) file.mkdirs();
    }

    @Bean
    JRFileVirtualizer fileVirtualizer() {
        return new JRFileVirtualizer(100, workPath);
    }

    @Bean
    JRSwapFileVirtualizer swapFileVirtualizer() {
        JRSwapFile sf = new JRSwapFile(workPath, 1024, 100);
        return new JRSwapFileVirtualizer(20, sf, true);
    }

}
