package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

/**
 * REST controller for Test Camel.
 */
@RestController
@RequestMapping("/api")
public class SampleCamelResource {

    private final Logger log = LoggerFactory.getLogger(SampleCamelResource.class);

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ConsumerTemplate consumerTemplate;

    @PostMapping("/camel/test")
    @Timed
    public ResponseEntity<Void> baseTest() throws URISyntaxException {
        log.debug("REST request to any Camel Test");
        return new ResponseEntity<Void>(null, null, HttpStatus.OK);
    }
}
