package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.service.ReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/api/report")
public class ReportResource {
    protected Logger logger = Logger.getLogger(ReportResource.class.getName());

    @Autowired
    ReportUtils reportUtils;

    @GetMapping("/{name}/pdf")
    public ResponseEntity<InputStreamResource> getReportPDF(@PathVariable String name, @RequestParam Map<String, Object> request, HttpServletRequest servRequest) {
        Map<String, Object> param = reportUtils.getReportParam(servRequest);
        param.putAll(request);
        return reportUtils.generatePDF(name, name + ".pdf", param);
    }

    @GetMapping("/{name}/xlsx")
    public ResponseEntity<InputStreamResource> getReportXLSX(@PathVariable String name, @RequestParam Map<String, Object> request, HttpServletRequest servRequest) {
        Map<String, Object> param = reportUtils.getReportParam(servRequest);
        param.putAll(request);
        return reportUtils.generateXLSX(name, name + ".xlsx", param);
    }

    @GetMapping("/{name}/csv")
    public ResponseEntity<InputStreamResource> getReportCSV(@PathVariable String name, @RequestParam Map<String, Object> request, HttpServletRequest servRequest) {
        Map<String, Object> param = reportUtils.getReportParam(servRequest);
        param.putAll(request);
        return reportUtils.generateCSV(name, name + ".csv", param);
    }

    @GetMapping("/{name}/html")
    public ResponseEntity<InputStreamResource> getReportHTML(@PathVariable String name, @RequestParam Map<String, Object> request, HttpServletRequest servRequest) {
        Map<String, Object> param = reportUtils.getReportParam(servRequest);
        param.putAll(request);
        return reportUtils.generateHTML(name, name + ".html", param);
    }
}
