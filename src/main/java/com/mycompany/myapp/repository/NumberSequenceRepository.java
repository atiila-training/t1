package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.NumberSequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Number Sequence entity.
 */

@Repository
public interface NumberSequenceRepository extends JpaRepository<NumberSequence, Long> {

    @Query("select r from NumberSequence r where (r.tag = :tag) and (r.value = :value)")
    NumberSequence queryNumber(@Param("tag") String tag, @Param("value") String value);

}
