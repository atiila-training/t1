package com.mycompany.myapp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Number Sequence Table.
 */

@Entity
@Table(name = "jhi_number_sequence")
public class NumberSequence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "value")
    private String value;

    @Column(name = "nextvalue")
    private Long nextValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getNextValue() {
        return nextValue;
    }

    public void setNextValue(Long nextValue) {
        this.nextValue = nextValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NumberSequence masterNumbering = (NumberSequence) o;
        if (masterNumbering.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), masterNumbering.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NumberSequence{" +
            "id=" + getId() +
            ", Tag='" + getTag() + "'" +
            ", Value='" + getValue() + "'" +
            ", nextValue=" + getNextValue() +
            "}";
    }
}
