@echo off
set tmpl=D:\apps\projects\jhipster-template
set src=d:\apps\projects\core-ng
set tgt=.
set entity=%1

set srcpathcode=%src%\src\main\java\id\atiila
set tgtpathcode=%tgt%\src\main\java\id\atiila
set testpathcode=%tgt%\src\test\java\id\atiila

copy %tmpl%\%entity%.json %tgt%\.jhipster\%entity%.json /y
call jhipster entity %1 --skip-install

