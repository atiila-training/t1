@echo off

cls

call mvnw spring-boot:run -Dmaven.test.skip=true -Dspring-boot.run.jvmArguments="-agentpath:d:/apps/tools/jrebel/lib/jrebel64.dll"