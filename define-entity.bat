@echo off
set tmpl=D:\apps\projects\jhipster-template
set tgt=.
set entity=%1


if not exist %tgt%\.jhipster\%entity%.json copy %tmpl%\%entity%.json %tgt%\.jhipster\%entity%.json /y
call jhipster entity %entity% --skip-install --skip-client --skip-server
copy %tgt%\.jhipster\%entity%.json %tmpl%\%entity%.json /y